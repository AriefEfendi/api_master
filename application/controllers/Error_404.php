<?php

/**

 * @Author: Arif Efendi - Kazuya Media Indonesia

 * @Date:   2021-05-19 00:46:45

 * @Last Modified by:   kazuya

 * @Last Modified time: 2021-05-19 12:53:26

 */

defined('BASEPATH') OR exit('No direct script access allowed');



class Error_404 extends CI_Controller {



	public function __construct()

	{

		parent::__construct();

		//Load Dependencies

	}



	// List all your items

	public function index()

	{

		$this->output->set_status_header('404');

		$this->load->view('errors/_dataErrors404');

	}

}



/* End of file Error_404.php */

/* Location: ./application/controllers/Error_404.php */

