<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @Author: Arif Efendi - Kazuya Media Indonesia
 * @Date:   2021-06-01 10:37:21
 * @Last Modified by:   kazuya
 * @Last Modified time: 2021-08-09 14:43:40
 */

class UploadFile extends BD_Controller {

  function __construct()
  {
    parent::__construct();
    $this->auth();
    $this->load->model('DashboardModel','model');
  }

  public function doUpload_post() {
  	$data = array (
  		'KTP_SIM' 	=> $this->input->post('KTP_SIM'),
  		'NAMADEPAN' => $this->input->post('NAMA_LENGKAP'),
  		'FOTOKTP' => $this->input->post('userfile'),
  	);



   	if($data['KTP_SIM'] != "") {
 	// configurasi Upload
   		$config['upload_path'] = 'upload/fotoktp/';
   		$config['allowed_types'] = 'gif|jpg|jpeg|png';
   	// $config['max_size'] = 1024 * 8;
   		$config['encrypt_name'] = TRUE;
   		$this->load->library('upload', $config);
   		$file_element_name = 'userfile';

	// Upload FOTOKTP
   		if ($this->upload->do_upload($file_element_name)) {
   			$uploadData = $this->upload->data();
   			$data['FOTOKTP'] = $uploadData['file_name'];
   		}
   		$this->model->insert($data);
    $status = REST_Controller::HTTP_OK; // OK (200) being the HTTP response code
} else {
    $status = REST_Controller::HTTP_NO_CONTENT; // NO_CONTENT (204).
}

   $res = array(
   // if status = 200  ||  status = 404 ( not found )
     "status"       => $status,
     "Data"         => $data,
   );

   echo json_encode($res);
 }
 



}
