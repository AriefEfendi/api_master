<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Person extends BD_Controller {

  function __construct()
  {
    parent::__construct();
    $this->auth();
    $this->load->model('M_person','model');
  }

  public function getdataAll_get() {
   $data = array(
    'field' => 'id',
    'id'    => $this->get('id'),
  );

   if ($data['id'] <> NULL)
   {
      // get data by id
     $record    = $this->model->_select( $data );
     $query     = $record->row_array();
     $status    = $this->res($query);
   } else {
      // get data all
     $record    = $this->model->_dataAll();
     $query     = $record->result_array();
     $status    = $this->res($query);
   }

   $res = array(
   // if status = 200  ||  status = 404 ( not found )
     "status"       => $status,
     "RecordsTotal" => $record->num_rows(),
     "Data"         => $query,
   );

   echo json_encode($res);
 }

 public function save_post() {
    $check = $this->model->checkId($this->post('email'));
    if($check == "OK") {
      $data = array(
        'name'  => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'fact'  => $this->input->post('fact')
      );
    // insert to table
      $this->model->insert($data);
    $status = REST_Controller::HTTP_OK; // OK (200) being the HTTP response code
  } else {
    $status = REST_Controller::HTTP_NO_CONTENT; // NO_CONTENT (204).
  }

  $res = array(
   "status"       => $status,
   "Data"         => $check,
 );

  echo json_encode($res);
}

 public function update_post() {
  $data = array(
    'id'    => $this->post('id'),
    'name'  => $this->post('name'),
    'email' => $this->post('email'),
    'fact'  => $this->post('fact')
  );
    // insert to table
  $this->model->update($data);
    $status = REST_Controller::HTTP_OK; // OK (200) being the HTTP response code

    $res = array(
     "status"       => $status,
     "Data"         => $data,
   );

    echo json_encode($res);
  }

public function delete_post() {
  $data   = array( 'id' => $this->post('id'));
  $record = $this->model->delete($data); // query delete.
  // NO_CONTENT (204) being the HTTP response code
  $status = REST_Controller::HTTP_NO_CONTENT;
  $res    = array ( "status" => $status, "data" => $record );
  echo json_encode($res);
}

public function checkdata_post() {
  $check = $this->model->checkId($this->input->post('name'));
  // check result
  ( $check == 'Data Sama' ) ? $status = REST_Controller::HTTP_NOT_FOUND : $status = REST_Controller::HTTP_OK;
  $res   = array ( "status" => $status, "data" => $check );
  echo json_encode($res);
}







} ?>
