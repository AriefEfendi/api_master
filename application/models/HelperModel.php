<?php class HelperModel extends CI_Model{
	public function getDataUrut($namaForm){
		$sql = "SELECT * FROM tb_urut WHERE namaForm='$namaForm' ";
		$query = $this->db->query($sql); return $query->result();
	}
	public function updateDataUrut($namaForm, $noUrut){
		$this->db->where('namaForm',$namaForm);
		$query=$this->db->update('tb_urut',array('noUrut'=>($noUrut+1)));
		return $query;
	}
	public function getNavbarParents($idjabatan) {
		$parent = " SELECT m1.menuparentid, m1.menuname, m1.menuid, (select m2.menuname from tb_menu m2 where m2.menuid=m1.menuparentid) AS menuparentname, tmj.`status`, tj.JABATAN, m1.menulink, m1.menuicon FROM tb_menu AS m1
		LEFT JOIN tb_menu AS m2 ON m1.menuid = m2.menuparentid
		INNER JOIN tb_menujabatan as tmj on tmj.menuid = m1.menuid
		INNER JOIN jabatan as tj on tj.ID = tmj.JABATANID
		WHERE tmj.`status`= 'YES' AND m1.menuparentid=m1.menuparentid
		AND tj.ID = '$idjabatan' AND m1.menuparentid='0'
		GROUP BY m1.menuid ORDER BY m1.sort ASC ";
		$query = $this->db->query($parent); return $query->result();
	}

}?>
