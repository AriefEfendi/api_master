-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2021 at 11:16 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_restci`
--

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

CREATE TABLE IF NOT EXISTS `cabang` (
  `ID` int(4) NOT NULL,
  `CABANG` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cabang`
--

INSERT INTO `cabang` (`ID`, `CABANG`) VALUES
(1, 'PUSAT');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
  `ID` int(4) NOT NULL,
  `IDLEVEL` int(4) NOT NULL,
  `JABATAN` varchar(100) NOT NULL,
  `KET` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`ID`, `IDLEVEL`, `JABATAN`, `KET`) VALUES
(1, 1, 'Admin', 'Staf Kuangan & CS');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `ID` int(11) NOT NULL,
  `LEVEL` varchar(10) DEFAULT NULL,
  `ICON` varchar(30) NOT NULL,
  `KATEGORI` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`ID`, `LEVEL`, `ICON`, `KATEGORI`) VALUES
(1, 'Admin', 'bx bx-user-pin', '1'),
(2, 'Manajer', 'bx bx-user-voice', '1'),
(3, 'SPV', 'bx bx-sitemap', '1'),
(4, 'Staf', 'bx bxs-group', '1'),
(5, 'Customer', 'bx', '1');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `IDPENGGUNA` varchar(11) NOT NULL,
  `IDJABATAN` varchar(11) NOT NULL,
  `IDCABANG` varchar(11) NOT NULL,
  `NAMADEPAN` varchar(50) DEFAULT NULL,
  `NAMABELAKANG` varchar(50) NOT NULL,
  `REKOMENDASI` varchar(11) NOT NULL,
  `HP` varchar(13) DEFAULT NULL,
  `TLP` varchar(12) DEFAULT NULL,
  `IDWILAYAH` varchar(15) NOT NULL,
  `KOTA` varchar(100) DEFAULT NULL,
  `KEC` varchar(100) DEFAULT NULL,
  `DESA` varchar(100) DEFAULT NULL,
  `ALMT` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(150) DEFAULT NULL,
  `STATUSAKUN` varchar(12) DEFAULT NULL,
  `NAMAPERUSAHAAN` varchar(150) DEFAULT NULL,
  `NPWP` varchar(16) DEFAULT NULL,
  `KTP_SIM` varchar(16) DEFAULT NULL,
  `FOTOKTP` varchar(50) NOT NULL,
  `JENISKELAMIN` varchar(2) NOT NULL,
  `JENISPELANGGAN` varchar(8) NOT NULL,
  `FOTO` varchar(30) NOT NULL,
  `inserted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`IDPENGGUNA`, `IDJABATAN`, `IDCABANG`, `NAMADEPAN`, `NAMABELAKANG`, `REKOMENDASI`, `HP`, `TLP`, `IDWILAYAH`, `KOTA`, `KEC`, `DESA`, `ALMT`, `EMAIL`, `STATUSAKUN`, `NAMAPERUSAHAAN`, `NPWP`, `KTP_SIM`, `FOTOKTP`, `JENISKELAMIN`, `JENISPELANGGAN`, `FOTO`, `inserted_at`) VALUES
('PG000001', '1', '1', 'Administrator', '', '', '1234567', '-', '3502170003', 'PONOROGO', 'PONOROGO', 'PAKUNDEN', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '2021-08-10 09:10:00');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
`id` int(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `fact` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `name`, `email`, `fact`) VALUES
(1, 'andiana', 'andiana@gmail.com', '1'),
(2, 'mirna', 'mirna8u@gmail.com', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_person`
--

CREATE TABLE IF NOT EXISTS `tb_person` (
`id` int(20) NOT NULL,
  `name` text,
  `address` text,
  `phone` text
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_person`
--

INSERT INTO `tb_person` (`id`, `name`, `address`, `phone`) VALUES
(1, 'Jhone doe', 'jl mt merbabu', '1234112233');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `IDUSERS` varchar(10) NOT NULL,
  `IDPENGGUNA` varchar(11) DEFAULT NULL,
  `USERNAME` varchar(30) DEFAULT NULL,
  `PASSWORD` varchar(250) DEFAULT NULL,
  `IDCABANG` int(11) DEFAULT NULL,
  `JABATANID` int(11) DEFAULT NULL,
  `AKSESANDROID` tinyint(1) NOT NULL,
  `ENTRYBY` varchar(30) DEFAULT NULL,
  `ENTRYDATE` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `LASTLOGIN` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `LASTIPADDR` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`IDUSERS`, `IDPENGGUNA`, `USERNAME`, `PASSWORD`, `IDCABANG`, `JABATANID`, `AKSESANDROID`, `ENTRYBY`, `ENTRYDATE`, `LASTLOGIN`, `LASTIPADDR`) VALUES
('6112b73b63', 'PG000001', 'tesapi', '763b9305e11e881935c89b51d8d577c30605909c', 1, 1, 1, 'PG000001', '2021-08-12 14:49:29', '2021-08-12 14:49:29', '127.0.0.1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
 ADD PRIMARY KEY (`IDPENGGUNA`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_person`
--
ALTER TABLE `tb_person`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`), ADD KEY `id_2` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`IDUSERS`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_person`
--
ALTER TABLE `tb_person`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
